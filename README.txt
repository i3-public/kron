
# 
# port: 4048
# wget --no-cache -qO- https://gitlab.com/i3-public/kron/-/raw/main/README.txt | bash

apt -y update
apt -y install wget

wget -qO- https://gitlab.com/i3-public/conf/-/raw/main/docker/README.txt | bash

docker rm -f kron
docker rmi kron-image
docker system prune -a -f

docker build -t kron-image https://gitlab.com/i3-public/kron/-/raw/main/conf/dockerfile
docker run -t -d --restart unless-stopped --name kron --hostname kron.i3ns.net -p 4048:4048 -p 4047:22 kron-image
