<?php

# 23th Dec, 2024

class arr {

    public static function is_associative( array $arr ){
        if (array() === $arr) return false;
        return array_keys($arr) !== range(0, count($arr) - 1);
    }
    
}

// class curl {

//     public static function post( $url , $params=null, $method="POST", $timeout=null ){

//         $ch = curl_init();
//         curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');
//         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

//         curl_setopt($ch, CURLOPT_URL, $url );
        
//         if( sizeof($params) ){
//             curl_setopt($ch, CURLOPT_POST, 1);
//             curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params) );
//         }

//         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//         if( $timeout ) curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);

//         $server_output = curl_exec ($ch);
//         curl_close ($ch);

//         return $server_output;

//     }

// }


class net {

    # 2024/11/19
    public static function wget( $url, $etc=[ /* post, timeout, user_agent, display_error */ ] ){

        if( array_key_exists('timeout', $etc) ){
            $http_arr['timeout'] = intval($etc['timeout']);
        }

        $user_agent = array_key_exists('user_agent', $etc) 
            ? $etc['user_agent'] 
            : 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36';
            
        $http_arr['header'] = "User-Agent: {$user_agent}\r\n";

        if( array_key_exists('post', $etc) and sizeof($etc['post']) ){
            $http_arr['header'].= 'Content-Type: application/x-www-form-urlencoded';
            $http_arr['method'] = 'POST';
            $http_arr['content'] = http_build_query($etc['post']);
        }

        $contx = stream_context_create([
            'http' => $http_arr,
            'ssl' => [ 'verify_peer'=>false, 'verify_peer_name'=>false ],
        ]);

        try {
            $res = file_get_contents($url, false, $contx);

        } catch (Exception $e) {
            if( array_key_exists('display_error', $etc) and $etc['display_error'] ){
                echo $e->getMessage();

            } else {
                proc::error($code);
            }
        }

        return $res;
        
    }

    # 2024/11/19
    public static function wjson( $url, $etc=[ /* post, timeout, user_agent, display_error */ ], $die=null ){
        
        if(! $res = self::wget( $url, $post_params, $etc ) ){
            $code = "no content received";

        } else if(! json::is($res) ){
            $code = "the result is not in the format of json";

        } else if(! $res = json_decode(trim($res), true) ){
            $code = "can't decode the json";
            
        } else if( !array_key_exists('status', $res) or !array_key_exists('code', $res) ){
            $code = "parameters not properly defined in result";

        } else if( $res['status'] != 'OK' ){
            $code = $res['code'];

        } else {
            return $res['data'];
        }

        if( $die ){
            die($code);

        } else {
            return proc::error($code);
        }

    }

    # 2024/11/19
    public static function host_ip(){

        $ip = trim(shell_exec('hostname -I'), "\r\n\t ");

        if( strstr($ip, ' ') ){        
            $ip = explode(' ', $ip)[0]; 
        }
        
        return $ip;

    }

    # 2024/11/19
    public static function is_ip( $str ){
        $ret = filter_var($str, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4);
        return $ret;
    }

    # 2024/11/28
    public static function remote_ip(){
        return
        req::server('HTTP_CF_CONNECTING_IP') ?: (
            req::server('HTTP_TUNNEL') ?: (
                req::server('REMOTE_ADDR')
            )
        );
    }

}


class proc {

    private static $error_msg;

    public static function error( $value=null ){
        
        if(! is_null($value) ){
            self::$error_msg = $value;
        
        } else {
            $msg = self::$error_msg;
            self::$error_msg = '';
        }
        
        return $msg;

    }

    public static function func_semaphore_open( $lock_name ){

        $lock_file = '/tmp/func_semaphore_'.$lock_name;

        while( file_exists($lock_file) ){
            time::sleep(0.1);
        }

        return touch($lock_file);
        
    }

    public static function func_semaphore_close( $lock_name ){
        $lock_file = '/tmp/func_semaphore_'.$lock_name;
        if( file_exists($lock_file) ){
            if( unlink($lock_file) ){
                return true;
            }
        }
        return false;
    }

}




class code {

    public static function array_from_json( $json ){
    
        if( $json ){
            if( $json != 'null' ){
                $json = json_decode($json, true);
                if( sizeof($json) ){
                    return $json;
                }
            }
        }
    
        return [];
        
    }

    public static function start_with( $text, $start ){
        return ( substr($text, 0, strlen($start) ) == $start );
    }

}


class json {

    # 2024/12/23
    public static function die( array $arr ){
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($arr, JSON_PRETTY_PRINT);
        die;
    }
    
    # 2024/12/23
    public static function is( $text ){
        @json_decode($text);
        $res = json_last_error() === JSON_ERROR_NONE;
        return $res;
    }

    public static function log_n_die( $arr, $echo=null ){
        log::it( $arr );
        self::die( $echo ?: $arr );
    }


}


class time {

    # 2024/11/20
    public static function sleep( $sec ){
        $sec = round( $sec * 1000000 );
        usleep($sec);
    }

    # 2024/11/20
    public static function milli(){
        $milliseconds = round( microtime(true) * 1000);
        return $milliseconds;
    }

}


class log {

    # 2024/11/20
    public static function it( $code, $path='/tmp/log', $breakLine=true ){

        if( defined('DEBUG_MODE') and DEBUG_MODE === true ){

            if( is_object($code) ){
                $code = (array)$code;
            }

            if( is_array($code) ){
                $code = json_encode($code);
            }

            if( $breakLine ){
                $code.= "\n";
            }

            if( $fp = fopen($path, 'a+') ){
                fwrite($fp, $code);
                fclose($fp);
            }
        
        }

        return $code;

    }

    # 2024/11/20
    public static function dot( $code='.' ){

        if( strlen($code) > 1 ){
            for( $i=0; $i<strlen($code); $i++ ){
                self::dot( substr($code, $i, 1) );
            }
            return;
        }

        global $flag_log_dot_i;
        if( $flag_log_dot_i > 40 )
            $flag_log_dot_i%= 40;

        self::it($code, '/tmp/dot', !(++$flag_log_dot_i%40) );

    }
    
}


# 2024/11/25
class req {

    # 2024/11/28
    public static function get( $key ){
        return self::arr($key, $_GET);
    }

    # 2024/11/28
    public static function post( $key ){
        return self::arr($key, $_POST);
    }

    # 2024/12/23
    public static function argv( $i ){
        
        global $argv;
        
        if(! isset($argv) ){
            echo "Script was not run from the command line.\n";
            return false;

        } else {
            return self::arr($i, $argv);
        }

    }

    # 2024/11/28
    public static function server( $key ){
        return self::arr($key, $_SERVER);
    }

    # 2024/11/28
    private static function arr( $key, $_ARR ){

        if( is_array($key) ){
            foreach( $key as $k ){
                if(! self::arr($k, $_ARR) )
                    return false;
            }
            return true;
        }

        // if( !is_string($key) and !is_numeric($key) ){
        //     return false;
        // }

        if( !isset($_ARR) or !is_array($_ARR) or !array_key_exists($key, $_ARR) or !$value = $_ARR[ $key ] ){
            return null;

        } else {
            return $value;
        }
        
    }

}


