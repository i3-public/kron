<?php


# 2020/11/11
# iusd 1.2

 
function insert( $tb, $ar ){

	$k_str = '`'. implode('`,`', array_keys($ar) ) .'`';
	
	$v_s = array_values($ar);
	foreach( $v_s as $v ){
		$v_str[] = ( is_numeric($v) ? $v : "'".$v."'" );
	}
	$v_str = implode(',', $v_str);

	if(! dbq(" INSERT INTO `$tb` ($k_str) VALUES ($v_str) ") ){
		echo dbe();
		return false;

	} else {
		return true;
	}

}


function update( $tb, $update, $ar ){

}


function select( $tb, $ar=null, $od=null, $lm=null, $gr=null ){


	#
	# ar
	$ar_str = iusd_where($ar);
	

	#
	# od
	$od_str = "";
	if( $od and sizeof($od) ){
		foreach( $od as $od_k => $od_v ){
			$od_arr[] = "`$od_k` $od_v";
		}
		$od_str = " ORDER BY ".implode(', ', $od_arr);
	}


	#
	# gr
	$gr_str = "";


	#
	# lm
	$lm_str = "";
	if( $lm ){
		$lm_str = " LIMIT $lm ";
	}


	#
	# query
	$query = " SELECT * FROM `$tb` WHERE 1 $ar_str $od_str $gr_str $lm_str ";
	// echo $query."\n";
	if(! $rs = dbq($query) ){
		echo dbe();
		return false;

	} else {

		$rw_s = [];
		
		while( $rw = dbf($rs) ){
			$rw_s[] = $rw;
		}

		if( sizeof($rw_s) == 1 ){
			return $rw_s[0];
		
		} else {
			return $rw_s;
		}

	}

}


function delete( $tb, $ar, $lm ){


}


function iusd_where( $ar ){
	
	$ar_str = "";

	if( $ar and sizeof($ar) ){
		
		foreach( $ar as $ar_k => $ar_v ){

			$ar_k = '`'.$ar_k.'`';
			
			$elem = "=";
			
			if( is_array($ar_v) ){
				
				list($elem, $ar_v) = $ar_v;
				
				if( $elem == 'between' ){
					list($bet1, $bet2) = $ar_v;
					if(! is_numeric($bet1) ) $bet1 = "'".$bet1."'";
					if(! is_numeric($bet2) ) $bet2 = "'".$bet2."'";
					$elem = ' BETWEEN ';
					$ar_v = " $bet1 AND $bet2 ";
				
				} else if( in_array(strtolower(trim($elem)), ['in', 'not in']) ){
					$elem = ' '.strtoupper($elem).' ';
					
					if( is_array($ar_v) ){
						foreach( $ar_v as $ar_v_i => $ar_v_this ){
							if(! is_numeric($ar_v_this) ){
								$ar_v[ $ar_v_i ] = "'".$ar_v_this."'";
							}
						}
						$ar_v = "(".implode(',', $ar_v).")";

					} else {
						$ar_v = "( $ar_v )";
					}

				}

			}

			if( $elem != ' BETWEEN ' )
			if(! in_array(trim($elem), [ 'IN', 'NOT IN' ] ) )
			if(! is_numeric($ar_v) ){
				$ar_v = "'".$ar_v."'";
			}
			
			$ar_str.= " AND ".$ar_k.$elem.$ar_v;

		}

	}

	// echo $ar_str;die();
	return $ar_str;
	
}


