<?php


class s3 {

    // image, ws/meta
    public static function putFormFile( $input, $direction ){

    }

    public static function putFile( $local_file, $remote_direction ){
        
        $res = self::put($local_file, $remote_direction);

    }

    public static function putData( $data, $remote_file ){
        
        $name = basename($remote_file);
        $remote_direction = dirname($remote_file);

        $local_direction = '/tmp/s3-putData-'.rand(100000,999999);
        mkdir($local_direction);

        $local_file = $local_direction.'/'.$name;
        file_put_contents($local_file, $data);
        $res = self::put($local_file, $remote_direction);

        unlink($local_file);
        rmdir($local_direction);

        if( $res ){
            return "http://".FTPUT_HOST.":2052/{$remote_direction}/{$name}";
        
        } else {
            return '';
        }

    }

    private static function put( $local_file, $remote_direction ){

        $mkd = '';

        if(! $fp = ftp_connect(FTPUT_HOST) ){
            echo "Can't connect to file server";

        } else {

            ftp_login($fp, FTPUT_USER, FTPUT_PASS);
            ftp_pasv($fp, true);
            
            if(! file_exists($local_file) ){
                echo "local file does not exists";
            
            } else {

                $tmp_arr = explode('/', $remote_direction);

                for( $i=0; $i<sizeof($tmp_arr); $i++ ){
                    $mkd.= $tmp_arr[$i]."/";
                    @ftp_mkdir($fp, $mkd);
                }

                $remote_file = $remote_direction.'/'.basename($local_file);
                return ftp_put( $fp, $remote_file, $local_file, FTP_BINARY );
                    
            }

        }

        return '';
        
    }

}

