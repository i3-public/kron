#!/bin/sh

crontab <<EOF

#
# sys
* * * * *    sudo bash /var/www/cron.sh
* * * * *    sudo bash /var/www/html/net-usage/net-cron.sh 4048 eth0 10000

# 
* * * * *    sudo php /var/www/cron/pr4/m3u_parts/index.php

EOF

