<?php include_once('/var/www/inc/.php');


if( req::argv(1) != 'test' ){
    echo "wait for 50 seconds ..\n";
	sleep(50);
}


if(! $rs_cat = dbq(" SELECT `id` cat_id, `category_name` cat_name, `category_type` cat_type FROM `stream_categories` WHERE `parent_id`=0 ORDER BY `cat_order` ") ){
	echo dbe();

} else {

	while( $rw_cat = dbf($rs_cat) ) {

		extract($rw_cat);

		$m3u = '';
		$m3u_plus = '';
        $prefixed_m3u = '';
        $prefixed_m3u_plus = '';
		$json = [];

		# cat subs
		$rs_sub = dbq(" SELECT `id` sub_id, `category_name` sub_name FROM `stream_categories` WHERE `parent_id`=$cat_id ORDER BY `cat_order` ");
		if( dbn($rs_sub) ){
			while( $rw_sub = dbf($rs_sub) ){
				
                extract($rw_sub);
				
                list($m3u_this, $m3u_plus_this, $prefixed_m3u_this, $prefixed_m3u_plus_this, $json_this) = m3u_by_cat($sub_id, $cat_type, $sub_name);
                
                $m3u.= $m3u_this;
				$m3u_plus.= $m3u_plus_this;
                $prefixed_m3u.= $prefixed_m3u_this;
        		$prefixed_m3u_plus.= $prefixed_m3u_plus_this;
				$json = $json + $json_this;

			}
		}

		# cat etc
		list($m3u_this, $m3u_plus_this, $prefixed_m3u_this, $prefixed_m3u_plus_this, $json_this) = m3u_by_cat($cat_id, $cat_type, $cat_name);

		$m3u.= $m3u_this;
		$m3u_plus.= $m3u_plus_this;
        $prefixed_m3u.= $prefixed_m3u_this;
		$prefixed_m3u_plus.= $prefixed_m3u_plus_this;
		$json = $json + $json_this;

		$m3u_s[ $cat_id ] = [ $m3u, $m3u_plus, $prefixed_m3u, $prefixed_m3u_plus, $json ];

	}


    $s3_direction = 'kron/pr4/m3u_parts';
	$s3_path = 'http://'.FTPUT_HOST.':2052/'.$s3_direction.'/';
	$json_md5_code = md5(json_encode($m3u_s));
	
    if( 
		$old_md5_code = net::wget("{$s3_path}.json.md5") 
		and
		$old_md5_code == $json_md5_code
	){
		$res = 'hit';

	} else {

		s3::putData( $json_md5_code, $s3_direction.'/.json.md5' );
	
		# 
		# tmp dir
		$tmp = '/tmp/m3u_parts';
		@mkdir($tmp);
		chdir($tmp);
		
		@mkdir('m3u');
		@mkdir('m3u_plus');
		@mkdir('prefixed_m3u');
		@mkdir('prefixed_m3u_plus');
		@mkdir('json');

		foreach( $m3u_s as $cat_id => list($m3u, $m3u_plus, $prefixed_m3u, $prefixed_m3u_plus, $json) ){
			file_put_contents("m3u/{$cat_id}.m3u", $m3u);
			file_put_contents("m3u_plus/{$cat_id}.m3u", $m3u_plus);
			file_put_contents("prefixed_m3u/{$cat_id}.m3u", $prefixed_m3u);
			file_put_contents("prefixed_m3u_plus/{$cat_id}.m3u", $prefixed_m3u_plus);
			file_put_contents("json/{$cat_id}.json", json_encode($json, JSON_PRETTY_PRINT) );
		}
		
		shell_exec(" tar -cf .tar * ");
		s3::putFile( "{$tmp}/.tar", $s3_direction );
		s3::putData( md5_file('.tar'), $s3_direction.'/.tar.md5' );

		shell_exec(" cd /tmp; rm -rf {$tmp} ");
		$res = 'miss';

	}

	json::die([
		'res' => $res,
		'files' => [
			"{$s3_path}.json.md5",
			"{$s3_path}.tar.md5",
			"{$s3_path}.tar"
		]
	]);

}




function m3u_by_cat( $cat_id, $cat_type, $cat_name ){

	$m3u = '';
	$m3u_plus = '';
    $prefixed_m3u = '';
    $prefixed_m3u_plus = '';
	$json = [];
	
	if( $cat_type == 'series' ){
		$rs = dbq(" SELECT `id` stream_id, `stream_display_name` stream_name, `target_container` extn, `stream_icon` logo, `added`, `channel_id`, `movie_propeties` FROM `streams` WHERE `id` IN ( SELECT `stream_id` FROM `series_episodes` WHERE `series_id` in (SELECT `id` FROM `series` WHERE `category_id`=$cat_id) ) ORDER BY `streams`.`stream_display_name` ASC ");
	
	} else {
		$rs = dbq(" SELECT `id` stream_id, `stream_display_name` stream_name, `target_container` extn, `stream_icon` logo, `added`, `channel_id`, `movie_propeties` FROM `streams` WHERE `category_id`=$cat_id AND `stream_source`!='[]' ORDER BY `stream_display_name` ASC ");
	}

	$type_name = [
		'live' => 'Live Streams',
		'movie' => 'Movies',
		'series' => 'Series'
	];


	if( dbn($rs) ){

		while( $rw = dbf($rs) ){
			
			extract($rw);
			
			if( $extn )
				$extn = json_decode($extn, true)[0];
			
			$rating = 0;
			
			switch( $cat_type ){

				case 'live':
					// $line = "http://<PORTAL>:8080/<USERNAME>/<PASSWORD>/{$stream_id}";
					$line = "<LINE>/{$stream_id}";
					break;

				case 'movie':
					if( json::is($movie_propeties) ){
						$movie_propeties = json_decode($movie_propeties, true);
						$logo = $movie_propeties['cover_big'];
						if( array_key_exists('rating', $movie_propeties) and is_numeric($movie_propeties['rating']) ){
							$rating = $movie_propeties['rating'];
						}
					}
					// $line = "http://<PORTAL>:8080/movie/<USERNAME>/<PASSWORD>/{$stream_id}.{$extn}";
					$line = "<MOVIE>/{$stream_id}.{$extn}";
					break;

				case 'series':
					// $line = "http://<PORTAL>:8080/series/<USERNAME>/<PASSWORD>/{$stream_id}.{$extn}";
					$line = "<SERIE>/{$stream_id}.{$extn}";
					break;

			}

			$stream_original_name = $stream_name;

			$stream_name = strstr($stream_name, ": ")
				? substr( strstr($stream_name, ": "), 2 )
				: $stream_name;
				
            # 1. m3u
			$m3u.= "#EXTINF:-1,{$stream_name}".PHP_EOL;
			$m3u.= $line.PHP_EOL;

            # 2. m3u_plus
			$m3u_plus.= '#EXTINF:-1 tvg-id="" tvg-name="'.$stream_name.'" tvg-logo="'.$logo.'" group-title="'.$cat_name.'",'.$stream_name.PHP_EOL;
			$m3u_plus.= $line.PHP_EOL;
            
            # 3. prefixed m3u
			$prefixed_m3u.= "#EXTINF:-1,{$stream_original_name}".PHP_EOL;
			$prefixed_m3u.= $line.PHP_EOL;

            # 4. prefixed m3u plus
			$prefixed_m3u_plus.= '#EXTINF:-1 tvg-id="" tvg-name="'.$stream_original_name.'" tvg-logo="'.$logo.'" group-title="'.$cat_name.'",'.$stream_original_name.PHP_EOL;
			$prefixed_m3u_plus.= $line.PHP_EOL;

			$json[ $stream_id ] = [
				"name" => $stream_name,
				"stream_type" => $cat_type,
				"type_name" => $type_name[ $cat_type ],
				"stream_id" => strval($stream_id),
				"stream_icon" => $logo,
				"epg_channel_id" => $channel_id,
				"added" => strval($added),
				"category_name" => $cat_name,
				"category_id" => $cat_id,
				"series_no" => null,
				"live" => strval($cat_type == 'live' ? 1 : 0),
				"container_extension" => $extn,
				"custom_sid" => null,
				"tv_archive" => 0,
				"direct_source" => "",
				"tv_archive_duration" => 0,

				"rating" => $rating,
				"rating_5based" => round( $rating / 2, 1),

			];

		}

	}

	return [ $m3u, $m3u_plus, $prefixed_m3u, $prefixed_m3u_plus, $json ];

}


